let styles =  `
body {
    background-color: #111111;
}

tr, table, td {
    background-color: #161616;
    color: #ededed !important;
}

a, a:link {
    color: #ffffff !important;
}

a:hover, a:link:hover, a:active a:link:active {
    color: #ff6600 !important;
    text-decoration: none;
}

a:visited, a:link:visited {
    color: #ff6600 !important;
}

input, input:focus, textarea, textarea:focus {
    background-color: #1c1c1c;
    color: #dddddd;
    border: 1px solid #f4731d;
    outline: none;
}

input[type=submit] {
    cursor: pointer;
    background-color: #1c1c1c;
    width: 20%;
    padding: 3;
}

input[type=submit]:focus, input[type=submit]:hover {
    background-color: #f4731d;
    color: #dddddd;
    border: 1px solid #f4731d;
    outline: none;
}

b, .commtext, .toptext, .subtext, .commhead {
    color: #dedede !important;
}

.title {
    color: #f4731d;
}
`;

let styleSheet = document.createElement("style");
styleSheet.type = "text/css";
styleSheet.innerText = styles;
document.head.appendChild(styleSheet)
